<?php 
session_start();
class SetU {
    public $host='localhost';
    public $username='root';
    public $password='';
    public $dbname='test';
    
    public $dive;
    public $url;
    public $conn;
    public $viewDB;
    
    function __construct() {
        $this->conn = mysqli_connect($this->host,$this->username,$this->password,$this->dbname);
    }
}
class SetUChecker extends SetU {
    public function MY_SQL_() {
        if (mysqli_connect_errno()) {
            echo "Database Connect Failed : " . mysqli_connect_error();
        }
        else {
            echo "Database Connected.";
        }
        return $this->conn;
    }
    public function MY_SQLSELECT_($SqlCommand) {
        $SQL_ARRAY_ADD = array();
        echo $Sql = 'SELECT * FROM '.$SqlCommand;
        $Query = mysqli_query($this->conn,$Sql);
        while($Result=mysqli_fetch_array($Query,MYSQLI_ASSOC)) {
            array_push($SQL_ARRAY_ADD,$Result);
        }
        mysqli_close($this->conn);
        $encode = json_encode($SQL_ARRAY_ADD);
        $decode = json_decode($encode, true);
        print_r($decode);
    }
    public function MY_SQLSELECT_ARRAY_($SqlCommand,$CName) {
        $SQL_ARRAY_ADD = array();
        echo $Sql = 'SELECT * FROM '.$SqlCommand;
        $Query = mysqli_query($this->conn,$Sql);
        while($Result=mysqli_fetch_array($Query,MYSQLI_ASSOC)) {
            array_push($SQL_ARRAY_ADD,$Result[''.$CName.'']);
        }
        mysqli_close($this->conn);
        print_r($SQL_ARRAY_ADD);
    }
}
class SetUSQL extends SetUChecker {
    public function MY_SQL() {
        return $this->conn;
    }
    public function MY_SQLSELECT($SqlCommand) {
        $SQL_ARRAY_ADD = array();
        $Sql = 'SELECT * FROM '.$SqlCommand;
        $Query = mysqli_query($this->conn,$Sql);
        while($Result=mysqli_fetch_array($Query,MYSQLI_ASSOC)) {
            array_push($SQL_ARRAY_ADD,$Result);
        }
        mysqli_close($this->conn);
        $encode = json_encode($SQL_ARRAY_ADD);
        $decode = json_decode($encode, true);
        return  $decode;
    }
    public function MY_SQLSELECT_ARRAY($SqlCommand,$CRef,$CName) {
        $SQL_ARRAY_ADD = array();
        $Sql = 'SELECT * FROM '.$SqlCommand;
        $Query = mysqli_query($this->conn,$Sql);
        while($Result=mysqli_fetch_array($Query,MYSQLI_ASSOC)) {
            array_push($SQL_ARRAY_ADD,$SQL_ARRAY_ADD[$Result[''.$CRef.'']] = $Result[''.$CName.'']);
        }
        mysqli_close($this->conn);
        return $SQL_ARRAY_ADD;
    }
    public function TURN($CReturn) {
        return $this->viewDB = $CReturn;
    }
}
class SetPAGE extends SetUSQL {
    public function USINC($File) {
       include($File.'.php');
    }
    public function CD($CDB,$Cdisplay) {
      echo $CDB[$Cdisplay];   
    }
}
$MAIN = new SetPAGE();
?>